package com.qfjy.java2102.zuochengu;

import lombok.Data;

@Data
//给所有control去用；
public class ResponseBo {
    /**
     * 响应的状态码  比如1001  1002
     */
    private String code;

    /**
     * 响应的描述星系
     */
    private String msg;
    /**
     * 响应的数据 比如list
     */
    private Object obj;

    public ResponseBo() {
    }

    public ResponseBo(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseBo(String code, String msg, Object obj) {
        this.code = code;
        this.msg = msg;
        this.obj = obj;
    }

    public static ResponseBo success() {
        return new ResponseBo(ResponseCodeEnum.SUCCESS.getCode(), ResponseCodeEnum.SUCCESS.getMsg());
    }

    public static ResponseBo success(Object obj) {
        return new ResponseBo(ResponseCodeEnum.SUCCESS.getCode(), ResponseCodeEnum.SUCCESS.getMsg(), obj);
    }

    public static ResponseBo failure() {
        return new ResponseBo(ResponseCodeEnum.FAILURE.getCode(), ResponseCodeEnum.FAILURE.getMsg());
    }
    public static ResponseBo success_code() {
        return new ResponseBo(ResponseCodeEnum.SUCCESS_CODE.getCode(), ResponseCodeEnum.SUCCESS_CODE.getMsg());
    }
    public static ResponseBo success_f() {
        return new ResponseBo(ResponseCodeEnum.SUCCESS_F.getCode(), ResponseCodeEnum.SUCCESS_F.getMsg());
    }




    @Override
    public String toString() {
        return "ResponseBody{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", obj=" + obj +
                '}';
    }
}
