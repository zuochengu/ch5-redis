package com.qfjy.java2102.zuochengu;


import com.alibaba.fastjson.JSON;
import com.qf.entity.po.UserPO;
import com.qf.service.impl.UserCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

@Controller
public class LoadController {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Autowired
    private UserCodeService userService;

    /**
     * 通过访问load。进入登录主页
     * @return
     */
    @RequestMapping("load")
    public String load(){
        return "index";
    }

    /**
     * 通过点击按钮获取验证码访问的请求
     * @param ip 地址
     * @param phone 手机号码
     * @return
     */
    @RequestMapping("get")
    @ResponseBody
    public String getPhoneCode(String phone, HttpServletRequest request) {
        String ip = request.getLocalAddr();

        Integer phoneCode = userService.getPhoneCode(ip, phone);
        if (!StringUtils.isEmpty(phoneCode.toString())){
            return JSON.toJSONString(ResponseBo.success());
        }
        return JSON.toJSONString(ResponseBo.failure());
    }

    /**
     * 用户通过注册提交按钮，后台判断正确与否。
     * @param phone 用户输入的手机号
     * @param authCode 用户输入的额验证码
     * @param request
     * @return
     */
    /**
     *判断用户请求的信息，与redis查询进行比较，如果相同返回true
     * @param ip
     * @param phone
     * @return
     */
    @RequestMapping("toLoad")
    @ResponseBody
    public String checkCode(String phone,String authCode,HttpServletRequest request){
        String ip = request.getLocalAddr();
        Boolean aBoolean = userService.checkCode(ip, phone, Integer.parseInt(authCode));
        if (aBoolean){

                return JSON.toJSONString(ResponseBo.success());
            }else {
            redisTemplate.opsForHash().increment(UserPO.getKeyName()+ip,"cout",1);
            Object cout =  redisTemplate.opsForHash().get(UserPO.getKeyName()+ip, "cout");
            if (Integer.parseInt(String.valueOf(cout))>3){
                redisTemplate.expire(UserPO.getKeyName(),20, TimeUnit.MINUTES);
                return JSON.toJSONString(ResponseBo.success_f());
            }
        }

        return JSON.toJSONString(ResponseBo.success_code());
    }
}
