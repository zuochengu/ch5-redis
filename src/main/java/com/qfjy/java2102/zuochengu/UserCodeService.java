package com.qfjy.java2102.zuochengu;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Random;
import java.util.concurrent.TimeUnit;


@Service
public class UserCodeService {
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    @Resource(name = "redisTemplate")
    private ValueOperations<String,String> string;

    /**
     *判断用户请求的信息，与redis查询进行比较，如果相同返回true
     * @param ip
     * @param phone
     * @return
     */
    public Boolean checkCode(String ip,String phone,Integer authCode){
        Boolean tmp = false;
        if (redisTemplate.opsForHash().hasKey(UserPO.getKeyName(),ip)){
            UserPO userPO = (UserPO) redisTemplate.opsForHash().get(UserPO.getKeyName(), ip);
            if (userPO.getPhoneCode().equals(authCode)&&userPO.getPhone().equals(phone)){

                return  tmp = true;
            }
        }
        return tmp;
    }
    /**
     * 判断手机号是否存在，存在就从redis查，不存在：就存在并且设置过期时间
     * @return
     */
    public Integer getPhoneCode(String ip, String phone) {
        if (redisTemplate.opsForHash().hasKey(UserPO.getKeyName(), ip)) {
            UserPO userPO = (UserPO) redisTemplate.opsForHash().get(UserPO.getKeyName(), ip);
            System.out.println(userPO.getPhoneCode());
            return userPO.getPhoneCode();
        } else {
            UserPO userPO = new UserPO();
            userPO.setIp(ip);
            userPO.setPhone(phone);
            userPO.setPhoneCode(UserCodeService.getRandomCode());
            redisTemplate.opsForHash().put(UserPO.getKeyName(), ip, userPO);
            redisTemplate.expire(UserPO.getKeyName(),120, TimeUnit.SECONDS);
            System.out.println(userPO.getPhoneCode());
            return userPO.getPhoneCode();
        }
    }
    /**
     * 返回一个随机生成的4位数的验证码
     * @return
     */
    public static Integer getRandomCode(){
        return new Random().nextInt(9999)+1000;
    }

}
