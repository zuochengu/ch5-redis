package com.qfjy.java2102.zuochengu;

public enum ResponseCodeEnum {
    SUCCESS("200", "请求成功"),
    SUCCESS_CODE("501","验证码错误"),
    SUCCESS_F("502","验证码次数已超过3次，请20分钟过后重新登录"),
    FAILURE("500", "系统错误");
    private String code;
    private String msg;


    ResponseCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
