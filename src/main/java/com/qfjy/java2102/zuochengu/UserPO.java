package com.qfjy.java2102.zuochengu;

import lombok.Data;

import java.io.Serializable;

/**
 * ip：用户的地址
 * phone：用户的登录的手机号
 * phoneCode:用户的登录验证码
 */
@Data
public class UserPO implements Serializable {
    private String ip;
    private String phone;
    private Integer phoneCode;
    private Integer count;
    public static String getKeyName(){return "usrPO";}
}
