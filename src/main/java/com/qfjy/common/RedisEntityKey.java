package com.qfjy.common;

public interface RedisEntityKey {

    String REDIS_ENTITY_KEY_USERPO="userpo";
    String REDIS_ENTITY_KEY_USER="user";
    String REDIS_ENTITY_KEY_ROLE="role";
}
