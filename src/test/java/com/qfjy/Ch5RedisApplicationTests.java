package com.qfjy;

import com.qfjy.entity.po.UserPO;
import com.qfjy.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Ch5RedisApplicationTests {


    @Autowired
    private UserServiceImpl userService;

    @Test
    void contextLoads() {
        userService.testRedis();
    }

    /**
     * 测试Redis  String
     */
    @Test
    void testString(){
       String result= userService.getString("nj:java:gradeName");
        System.out.println(result);
    }

    /**
     * 测试Redis hash
     */
    @Test
    void testHash(){
       UserPO userPO= userService.selectUserPOById("10001");
        System.out.println(userPO);
    }
}
